var gulp = require('gulp');
var concat = require('gulp-concat');
var critical = require('grunt-criticalcss');
var minify = require('gulp-minify');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');
var del = require('del');





gulp.task('pack-js-main', [], function () {
    return gulp.src([
        'src/js/jquery-3.1.0.min.js',
        'src/js/bootstrap.min.js',
        'src/js/bootstrap-filestyle.min.js',
        'src/js/bootstrap-multiselect.min.js',
        'src/js/nouislider.min.js',
        'src/js/slick.min.js',
        'src/js/fancybox/jquery.fancybox.pack.js',
        'src/js/functions.js'
    ])
        .pipe(concat('mainjs.js'))
        .pipe(minify({
            ext:{
                min:'.js'
            },
            noSource: true
        }))
        //.pipe(rev())
        .pipe(gulp.dest('src/public'));
});


gulp.task('pack-css-main', [], function () {
    return gulp.src([
        'src/css/font-awesome.min.css',
        'src/css/bootstrap.min.css',
        'src/css/bootstrap-multiselect.min.css',
        'src/js/fancybox/jquery.fancybox.css',
        'src/css/nouislider.min.css',
        'src/css/slick.min.css',
        'src/css/styles.css'
    ])
        .pipe(concat('maincss.css'))
        .pipe(cleanCss())
        .pipe(gulp.dest('src/public'));
});

gulp.task('critical', function (cb) {
    critical.generate({
        url: "https://beta.memorabbit.pl",
        width: 1920,
        height: 1080,
        outputfile: "src/public/critical.css",
        filename: "src/public/maincss.css", // Using path.resolve( path.join( ... ) ) is a good idea here
        buffer: 800*1024,
    });
});

gulp.task('default', ['pack-js-main', 'pack-css-main']);//'pack-js-main',