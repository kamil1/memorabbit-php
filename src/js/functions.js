function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function hideCookieInfo()
{
    if(readCookie('cookie-info') != 1)
    {
           $('.cookies-info').show();
    }
}




$(document).ready(function(){

	$(function () {
		$('[data-toggle="tooltip"]').tooltip({
			html: true
		});
	})
    $('[data-toggle="popover"]').popover({
        trigger : 'hover',
        container: 'body'
    });
	
	$('.navbar-toggle').click(function(){
		$('.sub-menu').hide();
	});

	$('.show-submenu').click(function(e){
		e.preventDefault();
		$('.sub-menu').slideToggle();
	});

	$('.show-search').click(function(e){
		e.preventDefault();
		$('.mobile-search').slideToggle();
	})

	$('.accessories .thumbnail').click(function(){
		$(this).toggleClass('active');
	})

	$('.footer-box .head').click(function(){
		$(this).parent().fadeOut();
	});


    hideCookieInfo();
	$('.cookies-info .btn').click(function(e){
		e.preventDefault();
		$('.cookies-info').fadeOut();
        createCookie('cookie-info',1, 3660);
	});

	$('.btn-color').on('click', function(){
		$('.btn-color').removeClass('active');
		$(this).addClass('active');
		var image = $(this).attr('product-image');
		$('.product-image').attr('src', image);
		
		$('.btn-color').removeClass('active');
		$(this).addClass('active');
		var image = $(this).attr('product-color1');
		$('.product-color1').attr('src', image);

		var image2 = $(this).attr('product-color2');
		$('.product-color2').attr('src', image2);
		$('.product-color1').fadeOut();
		$('.product-color1').fadeIn();
		$('.product-color2').fadeOut();
		$('.product-color2').fadeIn();
		$('.product-base').fadeOut();
		$('.product-base').fadeIn();
	});

	/* $(document).ready(function() {
		$('#search-form #size').multiselect({
			buttonText: function(options, select) {
				return 'Rozmiar';
			}
		});
		$('#search-form #marking').multiselect({
			buttonText: function(options, select) {
				return 'Znakowanie';
			}
		});
		$('#search-form #color').multiselect({
			buttonText: function(options, select) {
				return 'Kolor';
			}
		});
		$('#search-form #price').multiselect({
			buttonText: function(options, select) {
				return 'Cena';
			}
		});
	}); */

	$(window).scroll(function(){
		var scroll = $(window).scrollTop();
		if(scroll > 200) {
			$('.scroll-top').fadeIn();
		}
		else {
			$('.scroll-top').fadeOut();
		}
	});

	$('.scroll-top').click(function(e){
        e.preventDefault();
		$('html, body').animate({
			scrollTop: 0
		}, 500);
	});

	$('.home1').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 6000
	});

	$('.home2').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 6000
	});

	$('.home3').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 6000
	});

    $(".fancybox").fancybox();

    $('.product-gallery-controls img').click(function() {
        var show = $(this).attr('show-image');
        $('.product-gallery img').removeClass('active');
        $('.product-gallery img[image='+show+']').addClass('active');
    });
	
	$('input[type=file').filestyle({
		icon : false,
		buttonText : 'Wybierz',
		buttonName : 'btn-default',
		size : 'sm'
	});
});