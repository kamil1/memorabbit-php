<?php

/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 01/02/2017
 * Time: 21:11
 */
class Redirector
{
    function getDB()
    {
        $client = new MongoDB\Client("mongodb://localhost:27017");
        return $client->derentis_one;
    }
    function redirect($url,$c)
    {

        $redirectUrl = '';
        if(
            strpos($url, '/pendrive/') !== false
            &&
            $url !== '/pendrive/'
            &&
            $url !== '/tag/pendrive/'
            &&
            strpos($url, '/blog/') == false
            &&
            strpos($url, '/tag/') == false
        )
        {

            $redirectUrl = $this->pendrive($url);
        }
        else{
            $redirectUrl = $this->broadCatch($url);
            //if someone gives us an url without ending slash and...we need it let's check and add it =)
            if($redirectUrl == "")
            {
                if(!$this->endsWith($url,'/'))
                {
                    $redirectUrl = $this->broadCatch($url.'/');
                }
            }

        }

        //var_dump($url,$redirectUrl);die;
        if($redirectUrl !== "")
        {
            return $c['response']
                ->withStatus(301)
                ->withHeader('Location', $redirectUrl);
        }
        else
        {
            return $c['response']
                ->withStatus(404)
                ->withHeader('Location', '/notfound')
                ->write('Page not found');
        }

    }

    function pendrive($url)
    {
        preg_match('/(\/pendrive)\/(ds-)(\d+)(.*)/', $url, $output_array);
        $number = $output_array[3];
        $productShower = new ProductShower($this->getDB());
        $product = $productShower->getProductForRedirect("DS-".$number);
        if($product !== NULL){
            return '/product/'.$product['name_url_pl'];
        }else{
            $newNumber = sprintf("%04d", intval($number));
            $product = $productShower->getProductForRedirect("DS-".$newNumber);
            if($product !== NULL){
                return '/product/'.$product['name_url_pl'];
            }
        }
        return "";

    }

    function startsWith($haystack, $needle) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    function endsWith($haystack, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
    }

    function broadCatch($url)
    {
        $returnPath = "";
        switch($url){
            case "/c/elektronika-reklamowa/glosniki/2099":
                $returnPath = "/c/powerbanki-reklamowe/powerbanki-bluetooth-glosnik";
                break;
            case "/c/elektronika-reklamowa/glosniki/" :
                $returnPath = "/c/powerbanki-reklamowe/powerbanki-bluetooth-glosnik";
                break;
            case "/c/pendrive-reklamowe/dlugopisy-reklamowe":
                $returnPath = "/c/pendrive-reklamowe/dlugopisy";
                break;
            case "/tag/pendrive/":
                $returnPath = "/blog/tag/pendrive/";
                break;
            case "/page/2/":
                $returnPath = "/";
                break;
            case "/pendrive-z-logo/":
                $returnPath = "/blog/pendrive-z-logo/";
                break;
            case "/pamieci-usb-reklamowe/":
                $returnPath = "/blog/pamieci-usb-reklamowe/";
                break;
            case "/usb-karty-kredytowe-stan-magazynowy-w-72h/":
                $returnPath = "/blog/usb-karty-kredytowe-stan-magazynowy-w-72h/";
                break;

            case "/stan-magazynowy-pamieci-usb/":
                $returnPath = "/blog/stan-magazynowy-pamieci-usb/";
                break;
            case "/pendrive":
                $returnPath = "/c/pendrive-reklamowe";
                break;
            case "/pendrive/":
                $returnPath = "/c/pendrive-reklamowe";
                break;
            case "/cennik-agencyjny/":
                $returnPath = "/blog/cennik-agencyjny/";
                break;
            case "/cennik-klienta-koncowego/":
                $returnPath = "/blog/cennik-klienta-koncowego/";
                break;
            case "/pendrivy-usb-karty-kredytowe/":
                $returnPath = "/blog/pendrivy-usb-karty-kredytowe/";
                break;
            case "/klucze-usb-pamieci-pendrive-w-ksztalcie-klucza/":
                $returnPath = "/blog/klucze-usb-pamieci-pendrive-w-ksztalcie-klucza/";
                break;
            case "/bransoletki-usb/":
                $returnPath = "/blog/bransoletki-usb/";
                break;
            case "/usb-otwieracze-do-butelek-pendrive/":
                $returnPath = "/blog/usb-otwieracze-do-butelek-pendrive/";
                break;
            case "/usb-bizuteryjne-pendrive/":
                $returnPath = "/blog/usb-bizuteryjne-pendrive/";
                break;
            case "/wyjatkowe-usb/":
                $returnPath = "/blog/wyjatkowe-usb/";
                break;
            case "/oferta/":
                $returnPath = "/blog/oferta/";
                break;
            case "/opakowania-na-pamieci-pendrive-usb/":
                $returnPath = "/blog/opakowania-na-pamieci-pendrive-usb/";
                break;
            case "/blog/":
                $returnPath = "/blog";
                break;
            case "/kontakt/":
                $returnPath = "/contact";
                break;
            case "/pendrive-z-nadrukiem/":
                $returnPath = "/blog/pendrive-nadrukiem-logo/";
                break;
            case "/pendrive-z-grawerem/":
                $returnPath = "/blog/pendrive-grawerem-logo/";
                break;
            case "/pendrive-z-tloczeniem/":
                $returnPath = "/blog/pendrive-z-tloczeniem/";
                break;
            case "/pendrive-z-domingiem/":
                $returnPath = "/blog/pendrive-z-domingiem/";
                break;
            case "/kontakt":
                $returnPath = "/contact";
                break;
            case "/category/kompendium-wiedzy/":
                $returnPath = "/blog";
                break;
            case "/pendrive-reklamowe/":
                $returnPath = "/blog/pendrive-reklamowe/";
                break;
            case "/pendrivy-usb-karty-kredytowe/":
                $returnPath = "/blog/pendrivy-usb-karty-kredytowe/";
                break;
            case "/pendrivy-usb-karty-kredytowe//":
                $returnPath = "/blog/pendrivy-usb-karty-kredytowe/";
                break;
            case "/2015/12/wizytowka-w-formie-pendriva-dobry-pomysl/":
                $returnPath = "/blog/wizytowka-formie-pendriva-dobry-pomysl/";
                break;
            case "/author/administrator2/":
                $returnPath = "/";
                break;
            case "/category/bez-kategorii/":
                $returnPath = "/blog";
                break;
            case "/2015/12/reklama-dzwignia-handlu-czyli-dlaczego-warto-inwestowac-w-gadzety-reklamowe/":
                $returnPath = "/blog/reklama-dzwignia-handlu-czyli-dlaczego-warto-inwestowac-gadzety-reklamowe/";
                break;
            case "/2015/11/pendrivowe-gadzety/":
                $returnPath = "/blog/pendrivowe-gadzety/";
                break;
            case "/2015/09/co-wybralyby-twoje-dane-czyli-w-czym-przenosic-pliki/":
                $returnPath = "/blog/wybralyby-dane-czyli-czym-przenosic-pliki/";
                break;
            case "/2015/08/jak-zmienic-pendrivea-w-kosc-pamieci-ram-w-kilku-krokach/":
                $returnPath = "/blog/zmienic-pendrivea-kosc-pamieci-ram-kilku-krokach/";
                break;
            case "/2015/07/gadzet-gadzetowi-nierowny/":
                $returnPath = "/blog/gadzet-gadzetowi-nierowny/";
                break;
            case "/2015/06/jak-zabezpieczyc-dostep-do-danych-na-pendrive/":
                $returnPath = "/blog/zabezpieczyc-dostep-danych-pendrive/";
                break;
            case "/2015/05/5-przenosnych-pomyslow-na-twoj-pendrive/":
                $returnPath = "/blog/5-przenosnych-pomyslow-twoj-pendrive/";
                break;
            case "/2012/12/przemysl-pamieci-flashowych-w-roku-2013/":
                $returnPath = "/blog/przemysl-pamieci-flashowych-roku-2013/";
                break;
            case "/author/admin/":
                $returnPath = "/blog";
                break;
            case "/2012/09/czy-mozna-zarazic-sie-wirusem-uzywajac-pamieci-usb/":
                $returnPath = "/blog/mozna-zarazic-sie-wirusem-uzywajac-przenosnych-pamieci-usb/";
                break;
            case "/blog/page/2/":
                $returnPath = "/blog";
                break;
            case "/pendrive-kategoria/pendrive-metalowe/":
                $returnPath = "/c/pendrive-reklamowe/pamiec-usb-metalowa";
                break;
            case "/pendrive-kategoria/pendrive-skorzane/":
                $returnPath = "/c/pendrive-reklamowe/pamiec-usb-skorzana";
                break;
            case "/pendrive-kategoria/pendrive-plastikowy/":
                $returnPath = "/c/pendrive-reklamowe/pendrive-reklamowy-plast-alu";
                break;
            case "/pendrive-kategoria/pendrive-drewniane/":
                $returnPath = "/c/pendrive-reklamowe/pamiec-usb-z-drewna";
                break;
            case "/pendrive-kategoria/pendrive-mini/":
                $returnPath = "/c/pendrive-reklamowe/pamiec-usb-mini";
                break;
            case "/pendrive-kategoria/usb-dlugopisy/":
                $returnPath = "/c/pendrive-reklamowe/dlugopisy-reklamowe";
                break;
            case "/pendrive-kategoria/pendrive-opaski/":
                $returnPath = "/c/pendrive-reklamowe/paski-reklamowe-rozne-kolory";
                break;
            case "/2012/09/co-sprawia-iz-uzywanie-pamieci-usb-reklamowych-jest-lepsze-niz-plyt-cd-r-badz-dvd-r/":
                $returnPath = "/sprawia-iz-uzywanie-pamieci-usb-reklamowych-lepsze-niz-plyt-cd-r-badz-dvd-r/";
                break;
            case "/2011/12/jak-uniknac-podrobek-pendrive/":
                $returnPath = "/blog/uniknac-podrobek-pendrive/";
                break;
            case "/pendrive-kategoria/pendrive-karty-kredytowe/":
                $returnPath = "/c/pendrive-reklamowe/karty-z-nadrukiem-i-pamiecia-usb";
                break;
            case "/tag/ochrona-danych/":
                $returnPath = "/blog";
                break;
            case "/pendrive-kategoria/ksztalt_indywidualny/":
                $returnPath = "/c/pendrive-reklamowe/pendrive-o-indywidualnym-wygladzie";
                break;
            case "/category/bez-kategorii/page/2/":
                $returnPath = "/";
                break;
            case "/tag/cd/":
                $returnPath = "/blog";
                break;
            case "/tag/dvd/":
                $returnPath = "/blog";
                break;
            case "/tag/usb/":
                $returnPath = "/blog";
                break;
            case "/tag/zalety/":
                $returnPath = "/blog";
                break;
            case "/2012/09/co-sprawia-iz-uzywanie-pamieci-usb-reklamowych-jest-lepsze-niz-plyt-cd-r-badz-dvd-r/?replytocom=15":
                $returnPath = "/sprawia-iz-uzywanie-pamieci-usb-reklamowych-lepsze-niz-plyt-cd-r-badz-dvd-r/";
                break;
            case "/2012/09/co-sprawia-iz-uzywanie-pamieci-usb-reklamowych-jest-lepsze-niz-plyt-cd-r-badz-dvd-r/?replytocom=16":
                $returnPath = "/sprawia-iz-uzywanie-pamieci-usb-reklamowych-lepsze-niz-plyt-cd-r-badz-dvd-r/";
                break;
            case "/tag/bledna-pojemnosc-pendrive/":
                $returnPath = "/blog";
                break;
            case "/tag/fejki/":
                $returnPath = "/blog";
                break;
            case "/tag/podrobione-pendrive/":
                $returnPath = "/blog";
                break;
            case "/tag/dyski-hybrydowe/":
                $returnPath = "/blog";
                break;
            case "/tag/nand-flash/":
                $returnPath = "/blog";
                break;
            case "/tag/napedy-tasmowe/":
                $returnPath = "/blog";
                break;
            case "/tag/pamiec-flash/":
                $returnPath = "/blog";
                break;
            case "/tag/rozwoj-pamieci-w-roku-2013/":
                $returnPath = "/blog";
                break;
            case "/tag/ssd/":
                $returnPath = "/blog";
                break;
            case "/tag/jak-sie-bronic/":
                $returnPath = "/blog";
                break;
            case "/tag/klucz-usb/":
                $returnPath = "/blog";
                break;
            case "/tag/virus/":
                $returnPath = "/blog";
                break;




        }

        return $returnPath;
    }
}