<?php

$externalConfig = require '../../config.php';
class MeteorWorks
{
    private $db = null;
    function __construct($db) {
        $this->db = $db;
    }
    function getExternalConfig()
    {
        global $externalConfig;
        return $externalConfig;
    }

    /**
     * Encrypts password with SHA256
     * @param $stringPass
     * @return string
     */
    private function getCryptedPass($stringPass)
    {
        return hash('sha256', $stringPass);
    }

    /**
     * Gets hashed password - final stored in db salt
     * @param $cryptedPass
     * @return string
     */
    private function getHashedPass($cryptedPass)
    {
        $rounds = 10;
        $salt = "";
        $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
        for($i=0; $i < 22; $i++) {
            $salt .= $salt_chars[array_rand($salt_chars)];
        }
        return crypt($cryptedPass, sprintf('$2a$%02d$', $rounds) . $salt);
    }

    /**
     * Checks if our pass is like the stored hash - crazy but works =)
     * @param $pass
     * @param $hash
     * @return bool
     */
    private function checkPass($pass, $hash)
    {
        return password_verify($pass, $hash);
    }

    function checkPassword($email, $pass)
    {
        $user = $this->db->users->findOne(["emails.address" => $email]);
        $aaa = $user['services']['password']['bcrypt'];
       // var_dump($aaa);die;
        $cryptPass = $this->getCryptedPass($pass);
        $wwww =  $this->checkPass($cryptPass, $aaa);
        var_dump($pass,$cryptPass,$aaa,$wwww);die;
        //$hash = $user[]
    }

}