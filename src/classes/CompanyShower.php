<?php

/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 06/01/2017
 * Time: 14:00
 */
class CompanyShower
{
    private $db = null;
    private $limit = 18;
    function __construct($db) {
        $this->db = $db;
    }

    function getCompany($id)
    {
        return $this->db->Contacts_Company->findOne(
            ['_id' => $id]);

    }
}