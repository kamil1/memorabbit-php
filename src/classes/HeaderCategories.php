<?php

/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 09/10/2016
 * Time: 16:45
 */
class HeaderCategories
{
    private $db = null;

    function __construct($db) {
        $this->db = $db;
    }

    function getCategoryRenderArray($entry)
    {
        return  array(
            "_id" => (string)$entry['_id'],
            "name" => $entry['name'],
            "parentId" => $entry['parentId'],
            "description" => $entry['description_pl'],
            "name_url" => $entry['name_url_pl'],
            "name_title" => $entry['name_title_pl'],
            "icon" => isset($entry['icon']) ? $entry['icon'] : ''

        );
    }

    function getGlobalCategories()
    {
        $result = $this->db->Product_Category->find(['parentId'=> '', 'active' => true, '_type' => 'category-memorabbit']);
        $finalArray = [];
        foreach ($result as $entry) {

            $array = $this->getCategoryRenderArray($entry);
            $subCategories = $this->getCategoriesByParentId((string)$entry['_id']);
            $finalArraySub = [];
            foreach ($subCategories as $entry) {
                $finalArraySub[] = $this->getCategoryRenderArray($entry);
            }
            $array['subCategories'] = $finalArraySub;
            $finalArray[] = $array;
        }

        return $finalArray;
    }

    function getCategoriesByParentId($parentId)
    {
        $cursor = $this->db->Product_Category->find(['parentId' => $parentId],['sort' => ["name" => 1]]);
        $result = [];
        foreach ($cursor as $object) {
            $result[] = $object;
        }

        return $result;
    }
}