<?php
$externalConfig = require '../../config.php';
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 09/10/2016
 * Time: 12:33
 */
class ProductShower
{
    private $db = null;
    private $limit = 18;
    function __construct($db) {
        $this->db = $db;
    }
    function getExternalConfig()
    {
        global $externalConfig;
        return $externalConfig;
    }
    function __getFromArray($array, $field, $defaultValue = '')
    {
        if (isset($array[$field]) && !empty($array[$field]))
        {
            return $array[$field];
        }
        else
        {
            return $defaultValue;
        }
    }

    function getProductForRedirect($currentProductName)
    {
        $entry = $this->db->Product->findOne(
            ['name' => $currentProductName],
            [
                'sort' => ['name' => -1]
            ]);
        return $entry;
    }



    function pagination($import, $type, $category, $page)
    {

        //$cursor = $this->getCount($import, $type, $category, $page);
        $total= $this->getCount($import, $type, $category, $page);
        $howManyPagesWeHave = (int) ceil($total/$this->limit);
        $next  = $page == $howManyPagesWeHave ? $howManyPagesWeHave : ($page + 1);
        $prev  = $page == 0 ? 1 : ($page - 1);
        if($prev == 0) $prev = 1;

        $prev1 = (($page - 1) >= 1) ? $page - 1 : $page;
        $prev2 = (($page - 2) >= 1) ? $page - 2 : $page;
        $next1 = (($page + 1) <= $howManyPagesWeHave) ? $page + 1 : $page;
        $next2 = (($page + 2) <= $howManyPagesWeHave) ? $page + 2 : $page;

        return array(
            'next' => $next,
            'next1' => $next1,
            'next2' => $next2,
            'prev' => $prev,
            'prev1' => $prev1,
            'prev2' => $prev2,
            'page' => $page,
            'howManyPagesWeHave' => $howManyPagesWeHave,
            'parentCategory' => $type,
            'category' => $category,
            'import' => $import
        );
    }

    function getPreviousProductArray($currentProduct)
    {
        $entry = $this->db->Product->findOne(
            ['active' => true, 'name' => array('$lt' => $currentProduct['name']), "categoryId" => $currentProduct["category"]["_id"]],
            [
                'sort' => ['name' => -1]
            ]);
        return $this->createProductArray($entry);
    }

    function getNextProductArray($currentProduct){
        $entry = $this->db->Product->findOne(
            ['active' => true, 'name' => array('$gt' => $currentProduct['name']), "categoryId" => $currentProduct["category"]["_id"]],
            [
                'sort' => ['name' => 1]
            ]);

        return $this->createProductArray($entry);
    }

    function paginationSingleImportProduct($type, $category, $currentProduct)
    {

        //$cursor = $this->getCount($import, $type, $category, $page);
        //$total= $this->getCount($import, $type, $category, $page);
        $howManyPagesWeHave = 0;
        $next  = $this->getNextProductArray($currentProduct);
        $prev  = $this->getPreviousProductArray($currentProduct);

        $page = 0;
        return array(
            'next' => $next["_id"] == "" ? null : $next["name_url"],
            'prev' => $prev["_id"] == "" ? null : $prev["name_url"],
            'page' => $page,
            'howManyPagesWeHave' => $howManyPagesWeHave,
            'parentCategory' => $type,
            'category' => $category,
            'import' => 'import'
        );
    }

    function getSkip($page)
    {
        return ($page - 1) * $this->limit;
    }

    //----------pricelist

    function createProductImportPricelistArray($entry, $isVip)
    {
        $category = $this->getCategoryById($entry['categoryId']);
        $capacities = $this->getCapacitiesArray($this->__getFromArray($entry,'capacities', []));
        $margins = $this->getMarginAmountRangesArray($this->__getFromArray($entry,'importMoq', 0),$isVip);
        return array(
            "_id" => (string)$entry['_id'],
            "name" => $entry['name'],
            "description" => $entry['description_pl'],
            "weight" => $entry['weight'],
            "length" => $entry['length'],
            "width" => $entry['width'],
            "height" => $entry['height'],
            "name_url" => $entry['name_url_pl'],
            "meta_title" => $entry['meta_title_pl'],
            "meta_description" => $entry['meta_description_pl'],
            "colors" => $this->getColors($entry['colors']),
            "print_width" => $entry['print_width'],
            "print_height" =>$entry['print_height'],
            "material" =>$this->getMaterial($entry['materialId']),
            "photos" => $this->getPhotos($entry['_id']),
            "category" => $this->getCategoryRenderArray($category),
            "moq" => $this->__getFromArray($entry,'importMoq', 1),
            "is_printing" => $this->__getFromArray($entry,'is_printing', 0),
            "is_engraving" => $this->__getFromArray($entry,'is_engraving', 0),
            "is_doming" => $this->__getFromArray($entry,'is_doming', 0),
            "is_debossed" => $this->__getFromArray($entry,'is_debossed', 0),
            "capacities" => $capacities,
            "margins" => $margins,
            "prices" => $this->getPricingArray($capacities,$margins,$isVip)

        );
    }

    function getImportPricelistCursor()
    {
        return $this->db->Product->find(
            ['active' => true, 'brand' => $this->getExternalConfig()['brand'], "_type" => array('$ne' => 'product-accessory')],
            [
                'sort' => ['name' => 1]
            ]);
    }


    function getImportPricelistArray($companyId)
    {
        $result = $this->getImportPricelistCursor();
        $finalArray = [];
        foreach ($result as $entry) {

            $finalArray[] = $this->createProductImportPricelistArray($entry, true); //correct isVip @todo
        }

        return $finalArray;
    }

    /**
     * Gets capacities that work for current model
     * @param ids $
     * @return array
     */
    function getCapacitiesArray($ids)
    {

        $result = $this->db->Product_Capacity->find(
            ['_id' => array('$in' => $ids)],
            []);
        $finalArray = [];
        foreach ($result as $entry) {

            $finalArray[] = $entry;
        }
        usort($finalArray, function($a, $b){
            //return strnatcmp($a['namePL'],$b['namePL']); //Case sensitive
            return strnatcasecmp($a->namePL,$b->namePL); //Case insensitive
        });

        return $finalArray;
    }
    function getMarginAmountRangesArray($minimalAmount, $isVip)
    {

        $result = $this->db->Pricelist_Import_Flashdrive_Others->findOne();

        if($isVip)
        {
            $result = $result->marginVip;
        }
        else
        {
            $result = $result->margin;
        }
        $realFinal = [];
        foreach ($result as $key => $element) {
            if ($element->to < $minimalAmount) {

            }
            else{
                $realFinal[] = $element;
            }
        }
        return $realFinal;

    }

    function getPricingArray($capacities, $margins, $isVip)
    {
        $result = [];
        $ahaha = $isVip == true ? "tak" : "nie";
        foreach($capacities as $capacity){
            foreach($margins as $margin) {
                $result[] = array("capacity" => $capacity->_id, "margin" => $margin->_id, "price" => rand(1, 15). " " . $capacity->namePL . " " . $margin->from . "-".$margin->to . " isVip " . $ahaha) ;
                }
        }


        return $result;
    }

    //---------end of pricelist



    function getCursor($import, $type, $category, $page = 1)
    {
        if($import == 'import') {
            $categories = $this->getCategoryIdsForQuery($type, $category);
            return $this->db->Product->find(
                ['active' => true, 'categoryId' => array('$in' => $categories)],
                [
                'sort' => ['name' => 1],
                'limit' => $this->limit,
                'skip' => $this->getSkip($page),
                ]);
        }else
        {
            return $this->getWarehouseCursor($import, $type, $category, $page);
        }
    }

    function getSearchCursor($text)
    {
        $searchQuery = array(
            'active' => true, 'brand' => 'memorabbit',
            '$or' => array(
                array(
                    'meta_description_pl' => new MongoDB\BSON\Regex(".*".$text.".*","i"),
                ),
                array(
                    'name' => new MongoDB\BSON\Regex(".*".$text.".*","i"),
                ),
                array(
                    'description_pl' => new MongoDB\BSON\Regex(".*".$text.".*","i"),
                ),
            )
        );

        return $this->db->Product->find(
            $searchQuery,
            [
                'sort' => ['name' => 1],
            ]);

    }

    /**
     * Returns warehouseProductItem ID
     * @param $productId
     * @return string
     */
    function getWarehouseProductId($productId)
    {
        $foundItem = $this->db->Warehouse_Product->findOne(['productId' => $productId]);
        if($foundItem == null)
        {
            return "";
        }
        else{
            return $foundItem['_id'];
        }
    }

    function getWarehouseCursor($import, $type, $category, $page = 1)
    {

        $categories = $this->getCategoryIdsForQuery($type, $category);
        $products = $this->db->Product->find(['active' => true,'categoryId' => array( '$in' => $categories)]);
        $finalArrayIds = [];
        foreach ($products as $entry) {
            $finalArrayIds[] = (string)$entry['_id'];
        }
        $warehouseProducts = $this->db->Warehouse_Product->find(['productId' => array( '$in' => $finalArrayIds)]);
        $finalArrayIds = [];
        foreach ($warehouseProducts as $entry) {
            $finalArrayIds[] = (string)$entry['productId'];
        }
        return $this->db->Product->find(
            ['active' => true,'_id' => array( '$in' => $finalArrayIds)],
            [
                'sort' => ['name' => 1],
                'limit' => $this->limit,
                'skip' => $this->getSkip($page),
            ]
        );
    }

    function getCount($import, $type, $category, $page = 1)
    {
        if($import == 'import')
        {
            $categories = $this->getCategoryIdsForQuery($type, $category);
            return $this->db->Product->count(['active' => true,'categoryId' => array( '$in' => $categories)]);
        }
        else
        {
            return $this->getCountWarehouse($import, $type, $category, $page);
        }

    }

    function getCountWarehouse($import, $type, $category, $page = 1)
    {
        $categories = $this->getCategoryIdsForQuery($type, $category);
        $products = $this->db->Product->find(['active' => true,'categoryId' => array( '$in' => $categories)]);
        $finalArrayIds = [];
        foreach ($products as $entry) {
            $finalArrayIds[] = (string)$entry['_id'];
        }
        return $this->db->Warehouse_Product->count(['productId' => array( '$in' => $finalArrayIds)]);
    }

    function createProductArray($entry,$import = 'import')
    {
        $category = $this->getCategoryById($entry['categoryId']);
        //isset($_POST['value']) ? $_POST['value'] : '';
        return array(
            "_id" => (string)$entry['_id'],
            "name" => $entry['name'],
            "description" => $entry['description_pl'],
            "weight" => $entry['weight'],
            "length" => $entry['length'],
            "width" => $entry['width'],
            "height" => $entry['height'],
            "usb3" => $this->__getFromArray($entry,'is_usb3', 0),
            "otg" => $this->__getFromArray($entry,'is_otg', 0),
            "name_url" => $entry['name_url_pl'],
            "meta_title" => $entry['meta_title_pl'],
            "meta_description" => $entry['meta_description_pl'],
            "colors" => isset($entry['colors']) ? $this->getColors($entry['colors']) : [],
            "additional" => isset($entry['additional']) ? $entry['additional'] : "",
            "print_width" => isset($entry['print_width']) ? $entry['print_width'] : 0,
            "print_height" =>isset($entry['print_height']) ? $entry['print_height'] : 0,
            "material" => isset($entry['materialId']) ? $this->getMaterial($entry['materialId']) : null,
            "photos" => $this->getPhotos($entry['_id']),
            "template" => $this->getTemplate($entry['_id']),
            "category" => $this->getCategoryRenderArray($category),
            "moq" => $this->__getFromArray($entry,'importMoq', 1),
            "is_printing" => $this->__getFromArray($entry,'is_printing', 0),
            "is_engraving" => $this->__getFromArray($entry,'is_engraving', 0),
            "is_doming" => $this->__getFromArray($entry,'is_doming', 0),
            "is_debossed" => $this->__getFromArray($entry,'is_debossed', 0),
            "warehouseProductId" => $import == 'warehouse' ? $this->getWarehouseProductId((string)$entry['_id']): ""

        );
    }

    function getBoxesForProductAsArray($product)
    {
        $cursor = $this->db->Product->find(["isBox" => true, "active" => true, "worksWith" => $product["_id"]]);
        $finalArray = [];
        foreach ($cursor as $entry) {

            $finalArray[] = $this->createProductArray($entry);
        }

        return $finalArray;
    }

    function getAccessoriesForProductAsArray($product)
    {
        $cursor = $this->db->Product->find(["isAccessory" => true, "active" => true, "worksWith" => $product["_id"]]);
        $finalArray = [];
        foreach ($cursor as $entry) {

            $finalArray[] = $this->createProductArray($entry);
        }

        return $finalArray;
    }

    function getSingleProduct($url)
    {
        $product = $this->db->Product->findOne(["name_url_pl" => $url]);
        $finalProduct =  $this->createProductArray($product);
        $finalProduct["boxes"] = $this->getBoxesForProductAsArray($finalProduct);
        $finalProduct["accesssories"] = $this->getAccessoriesForProductAsArray($finalProduct);
        $finalProduct["technical"] = $this->getTechnical($finalProduct);

        return $finalProduct;

    }


    function getSearchProductArray($text)
    {

        $result = $this->getSearchCursor($text);

        $finalArray = [];
        foreach ($result as $entry) {

            $finalArray[] = $this->createProductArray($entry);
        }

        return $finalArray;
    }

    function getProductArray($import, $type, $category, $page)
    {

        $result = $this->getCursor($import, $type, $category, $page);
        //$result = $_result->sort($sort)->skip($this->getSkip($page))->limit($this->limit);
        //$cursor->limit(10)->skip(20);
        //        $_cursor = $cursor->sort($sort)->skip($this->getSkip($page))->limit($this->limit);
        $finalArray = [];
        foreach ($result as $entry) {

            $finalArray[] = $this->createProductArray($entry,$import);
        }

        return $finalArray;
    }

    function getCategoryArray($import, $type, $current)
    {
        $parent = $this->getCategoryByUrl($type);
        $categories = $this->getCategoriesByParentId($parent['_id'],$import);
        $finalArray = [];
        foreach($categories as $entry)
        {
            $finalArray[] = array(
                "_id" => (string)$entry['_id'],
                "name" => $entry['name'],
                "parentId" => $entry['parentId'],
                "description" => $entry['description_pl'],
                "name_url" => $entry['name_url_pl'],
                "name_title" => $entry['name_title_pl'],
                "icon" => $entry['icon'],
                "active" => $current == $entry['name_url_pl'] ? true : false

            );
        }

        return $finalArray;
    }

    function getCategoryByUrlForRender($url)
    {
        $entry = $this->getCategoryByUrl($url);
        return  $this->getCategoryRenderArray($entry);
    }

    function getCategoryByIdForRender($id)
    {
        $entry = $this->getCategoryById($id);
        return  $this->getCategoryRenderArray($entry);
    }

    function getCategoryRenderArray($entry)
    {
        return  array(
            "_id" => (string)$entry['_id'],
            "name" => $entry['name'],
            "parentId" => $entry['parentId'],
            "description" => $entry['description_pl'],
            "name_url" => $entry['name_url_pl'],
            "name_title" => $entry['name_title_pl'],
            "icon" => isset($entry['icon']) ? $entry['icon'] : '',
            "h1" => isset($entry['name_h1_pl']) ? $entry['name_h1_pl'] : '',
            "h2" => isset($entry['name_h2_pl']) ? $entry['name_h2_pl'] : ''

        );
    }

    function getCategoryIdsForQuery($type, $category)
    {
        $finalArray = [];
        if(strlen($category)>0)
        {
            $result = $this->getCategoryByUrl($category);
            $finalArray[] = (string)$result['_id'];
        }
        else
        {
            $result = $this->getCategoryByUrl($type);
            $categories = $this->getCategoriesByParentId((string)$result['_id']);
            foreach ($categories as $category)
            {
                $finalArray[] = (string)$category['_id'];
            }
        }

        return $finalArray;
    }

    function getPhotos($productId)
    {

        $mainPhoto = $this->db->selectCollection( 'cfs.productImages.filerecord' )->findOne(['productId' => $productId, 'usage' => 'main']);

        $finalArray = [];
        $finalArray[] = array(
            "_id" => (string)$mainPhoto['_id'],
            "url" => str_replace ( 'productImages/' , '', $mainPhoto['copies']['productImages']['key']) . '/' . $mainPhoto['seoNamePL'],
            "type" => $mainPhoto['usage'],
            "alt" => $mainPhoto['altPL']

        );

        $secondary1Photo = $this->db->selectCollection( 'cfs.productImages.filerecord' )->findOne(['productId' => $productId, 'usage' => 'secondary']);
        if($secondary1Photo !== null)
        {
            $finalArray[] = array(
                "_id" => (string)$secondary1Photo['_id'],
                "url" => str_replace ( 'productImages/' , '', $secondary1Photo['copies']['productImages']['key']) . '/' . $secondary1Photo['seoNamePL'],
                "type" => $secondary1Photo['usage'],
                "alt" => $secondary1Photo['altPL']

            );
        }

        $secondary2Photo = $this->db->selectCollection( 'cfs.productImages.filerecord' )->findOne(['productId' => $productId, 'usage' => 'secondary'],['skip' => 1]);
        if($secondary2Photo !== null)
        {
            $finalArray[] = array(
                "_id" => (string)$secondary2Photo['_id'],
                "url" => str_replace ( 'productImages/' , '', $secondary2Photo['copies']['productImages']['key']) . '/' . $secondary2Photo['seoNamePL'],
                "type" => $secondary2Photo['usage'],
                "alt" => $secondary2Photo['altPL']

            );
        }


        return $finalArray;
    }

    function getTemplate($productId)
    {

        $mainPhoto = $this->db->selectCollection( 'cfs.productAttachments.filerecord' )->findOne(['productId' => $productId, 'usage' => 'template-pl']);
                $finalArray = [];
        if($mainPhoto != null)
        {
            $url = str_replace ( 'productAttachments/' , '', $mainPhoto['copies']['productAttachment']['key']);
            $url = str_replace(' ', '+', $url);
            $finalArray[] = array(
                "_id" => (string)$mainPhoto['_id'],
                "url" => $url,
                "type" => $mainPhoto['usage'],
                "alt" => "SZABLON"

            );
        }



        return $finalArray;
    }

    function getTechnical($productId)
    {
        $entries = $this->db->selectCollection( 'cfs.productImages.filerecord' )->find(['productId' => $productId, 'usage' => 'technical']);

        $finalArray = [];
        foreach($entries as $entry){
            $finalArray[] = array(
                "_id" => (string)$entry['_id'],
                "url" => str_replace ( 'productImages/' , '', $entry['copies']['productImages']['key']) . '/' . $entry['seoNamePL'],
                "type" => $entry['usage'],
                "alt" => $entry['altPL']

            );
        }

        return $finalArray;
    }

    function getCategoryById($id)
    {
        return $this->db->Product_Category->findOne(['_id' => $id]);
    }

    function getCategoryByUrl($categoryUrl)
    {
        return $this->db->Product_Category->findOne(['name_url_pl' => $categoryUrl]);
    }

    function getCategoriesByParentId($parentId,$import = 'import')
    {
        $cursor = $this->db->Product_Category->find(['parentId' => $parentId],[
            'sort' => ['name' => 1]
        ]);
        $result = [];
        $resultSecond = [];
        foreach ($cursor as $object) {
            if($object["is_accessory"] == true){
                $resultSecond[] = $object;
            }
            else{
                $result[] = $object;
            }

        }
        if($import == 'import'){
            foreach ($resultSecond as $object) {
                $result[] = $object;
            }
        }



        return $result;
    }

    function getColors($arrayOfIds, $import = '')
    {
        $finalArray = [];
        if($arrayOfIds == null)
        {
            $arrayOfIds = [];
        }
        foreach ($arrayOfIds as $entry) {
            $result = $this->db->Product_Color->findOne(['_id' => $entry]);

            $finalArray[] = array(
                "name" => $result['name'],
                "hash" => $result['hash']
            );
        }
        return $finalArray;
    }

    function getMaterial($materialId)
    {

        $result = $this->db->Product_Material->findOne(['_id' => $materialId]);

        $final = array(
            "name" => $result['name'],
            "meta_name" => $result['meta_name'],
            "icon" => $result['icon']
        );

        return $final;
    }
}