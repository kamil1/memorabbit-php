<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
session_start();
require '../vendor/autoload.php';
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

$externalConfig = require '../../config.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['db']['host']   = "localhost";
$config['db']['user']   = "user";
$config['db']['pass']   = "password";
$config['db']['dbname'] = "exampleapp";
$config['templates'] = getenv('SLIM_MODE') == 'production' ? '/var/www/memorabbit-php/src/templates/' : '/Users/kamil/PhpstormProjects/memorabbit-php/src/templates/';


$app = new \Slim\App(["settings" => $config]);
function tvar_dump($smth,$die = false){
    if($_COOKIE["test"]=='test'){
        var_dump($smth);
        if($die == true){
            die;
        }
    }
}
function getUrl($request){
    $uri = $request->getUri();

    return $uri->getScheme() . "://" . $uri->getHost() . $uri->getPath();
}
function getExternalConfig()
{
    global $externalConfig;
    return $externalConfig;
}

$container = $app->getContainer();

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['db'] = function ($container) {
    $client = new MongoDB\Client("mongodb://localhost:27017");
    if(getenv('SLIM_MODE') == 'production'){
        return $client->derentis_one;
    }
    else
    {
        return $client->derentisone;
    }

};

$container['view'] = function ($container) {
    $templatesDir = $container['settings']['templates'];
    $view = new \Slim\Views\Twig($templatesDir, [
        'cache' => false
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));

    return $view;
};

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $path = $request->getUri()->getPath();
        $redirector = new Redirector();
        //var_dump($path);var_dump($redirector->redirect($path,$c));die;
        return $redirector->redirect($path,$c);
    };
};

spl_autoload_register(function ($classname) {
    require ("../classes/" . $classname . ".php");
});

$app->get('/', function (Request $request, Response $response) {
    $headerCategories = new HeaderCategories($this->db);
    $response = $this->view->render($response, "index.phtml", [
        "router" => $this->router,
        "headerCategories" => $headerCategories->getGlobalCategories(),
        "currenturl" => getUrl($request),
        "externalConfig" => getExternalConfig()
    ]);

    return $response;
})->setName("home");
$app->get('/notfound', function (Request $request, Response $response) {

    $headerCategories = new HeaderCategories($this->db);
    $path = $request->getUri()->getPath();

    $response = $this->view->render($response, "notfound.phtml", ["router" => $this->router, "headerCategories" => $headerCategories->getGlobalCategories(),"externalConfig" => getExternalConfig()]);

    return $response->withStatus(404);
    //return $response;
})->setName("notfound");

$app->get('/contact', function (Request $request, Response $response) {
    $headerCategories = new HeaderCategories($this->db);
    $response = $this->view->render($response, "contact-page.phtml", ["router" => $this->router, "headerCategories" => $headerCategories->getGlobalCategories(),"breadcrumbtitle" => "Kontakt","externalConfig" => getExternalConfig()]);

    return $response;
})->setName("contact");

$app->post('/contact', function (Request $request, Response $response) {
    $headerCategories = new HeaderCategories($this->db);

    $contactform = $request->getParsedBody()['contactform'];
    $email = filter_var($contactform['email'], FILTER_SANITIZE_EMAIL);
    $nameSurname = filter_var($contactform['nameSurname'], FILTER_SANITIZE_STRING);
    $companyName = filter_var($contactform['companyName'], FILTER_SANITIZE_STRING);
    $typeOfEmail = filter_var($contactform['typeOfEmail'], FILTER_SANITIZE_STRING);
    $content = filter_var($contactform['content'], FILTER_SANITIZE_STRING);
    //$dataUsage = isset($contactform['allowPersonalData']) ?  'tak' : 'nie'; //we want it to be ON
    //@TODO need to add validation
    $httpClient = new GuzzleAdapter(new Client());
    $sparky = new SparkPost($httpClient, ['key'=>getExternalConfig()['emailProviderKey']]);
//biuro@memorabbit.pl'

    $sparky->setOptions(['async' => false]);
    $promise = $sparky->transmissions->post([
        'content' => [
            'from' => [
                'name' => 'formularz kontaktowy memorabbit',
                'email' => 'noreply@memorabbit.pl',
            ],
            'subject' => 'wiadomosc z formularza kontaktowego memorabbit',
            'html' => 'email: '.$email.'<br/>'.'imie i nazwisko: '.$nameSurname .'<br/>'.'Nazwa firmy: '.$companyName . '<br/>' . 'zawartosc: ' . $content,//. '<br/>zgoda na przetwarzanie danych: ' . $dataUsage,
        ],
        'return_path' => 'anylocalpart@bounces.memorabbit.pl',
        'recipients' => [
            [
                'address' => [
                    'email' => 'biuro@memorabbit.pl'
                ],
            ],
        ]
    ]);

    //$response = $promise->wait();
    /*try {

        //echo $response->getStatusCode()."\n";
        //print_r($response->getBody())."\n";


    } catch (\Exception $e) {
        //echo $e->getCode()."\n";
        //echo $e->getMessage()."\n";
    }*/


    $response2 = $this->view->render($response, "contact-page-thank-you.phtml", ["router" => $this->router, "headerCategories" => $headerCategories->getGlobalCategories(),"breadcrumbtitle" => "Kontakt","externalConfig" => getExternalConfig()]);

    return $response2;


})->setName("contact-post");
$app->post('/test', function (Request $request, Response $response) {


   // $meteor = new MeteorWorks($this->db);
    //return $meteor->checkPassword($request->getParsedBody()['email'],$request->getParsedBody()['pass']);

    /*
    if(!isset($_SESSION['test']))
    {
        $_SESSION['test'] = 0;
    }
    else{
        $_SESSION['test'] = $_SESSION['test'] + 1;
        if($_SESSION['test'] > 3)
        {
            unset( $_SESSION['test'] );
        }
    }
    //unset( $_SESSION[$sessVar] );

    var_dump($_SESSION['test']);die;
return $request->getParsedBody()['callFunc1'] + $_SESSION['test'];
    */

})->setName("contact-post");

$app->get('/category/warehouse/', function (Request $request, Response $response) {
    $headerCategories = new HeaderCategories($this->db);
    $categories = $headerCategories->getGlobalCategories();

    return $response->withStatus(301)->withHeader('Location', '/category/warehouse/'.$categories[0]['name_url'].'/1');
})->setName("page-privacy-policy");

$app->get('/privacy-policy', function (Request $request, Response $response) {
    $headerCategories = new HeaderCategories($this->db);
    $response = $this->view->render($response, "page-privacypolicy.phtml", ["router" => $this->router, "headerCategories" => $headerCategories->getGlobalCategories(),"breadcrumbtitle" => "Polityka prywatności","externalConfig" => getExternalConfig()]);

    return $response;
})->setName("page-privacy-policy");

$app->get('/product/{seourl}', function (Request $request, Response $response, $args) {

    $seourl = filter_var($args['seourl'], FILTER_SANITIZE_STRING);
    $productShower = new ProductShower($this->db);



    $headerCategories = new HeaderCategories($this->db);

    $product = $productShower->getSingleProduct($seourl);

    $category = $product["category"];
    $parentCategory = $productShower->getCategoryByIdForRender($category["parentId"]);
    $pagination = $productShower->paginationSingleImportProduct($parentCategory["name_url"], $category["name_url"], $product);


    $response = $this->view->render($response, "product-page.phtml", [
        "router" => $this->router,
        "product" => $product,
        "category" => $category,
        "parentCategory" => $parentCategory,
        "headerCategories" => $headerCategories->getGlobalCategories(),
        "pagination" => $pagination,
        "currenturl" => getUrl($request),
        "externalConfig" => getExternalConfig()

    ]);

    return $response;
})->setName("product");
$app->get('/c/{parentCategoryUrl}/', function (Request $request, Response $response, $args) {
    $path = $request->getUri()->getPath();
    $path = substr($path, 0, -1);
    return $this['response']
        ->withStatus(301)
        ->withHeader('Location', $path);
})->setName("categoryGlobalSlash");
$app->get('/c/{parentCategoryUrl}[/{page:[0-9]+}]', function (Request $request, Response $response, $args) {
    $productShower = new ProductShower($this->db);
    $headerCategories = new HeaderCategories($this->db);
    $import = filter_var($request->getAttribute('import', 'import'), FILTER_SANITIZE_STRING);
    $parentCategoryUrl = filter_var($args['parentCategoryUrl'], FILTER_SANITIZE_STRING);
    $categoryUrl = "";
    $page = filter_var($request->getAttribute('page', 1), FILTER_VALIDATE_INT);

    $products = $productShower->getProductArray($import, $parentCategoryUrl, $categoryUrl, $page);
    $parentCategory = $productShower->getCategoryByUrlForRender($parentCategoryUrl);
    $currentCategory = null;
    $categories = $productShower->getCategoryArray($import, $parentCategoryUrl, $categoryUrl);
    $pagination = $productShower->pagination($import, $parentCategoryUrl, $categoryUrl, $page);

    if($page > 1 && count($products) == 0)
    {
        $path = "/c/".$parentCategoryUrl;
        return $this['response']
            ->withStatus(301)
            ->withHeader('Location', $path);
    }

    $whatPage = $import == 'import' ? "products-page-only-parent-category.phtml" : "products-page-only-parent-category-warehouse.phtml";
    $response = $this->view->render($response, $whatPage, [
        "router" => $this->router,
        "products" => $products,
        "pagination" => $pagination,
        "categories" => $categories,
        "category" => $currentCategory,
        "import" => $import,
        "parentCategory" => $parentCategory,
        "headerCategories" => $headerCategories->getGlobalCategories(),
        "currenturl" => getUrl($request),
        "externalConfig" => getExternalConfig()
    ]);
    return $response;
})->setName("categoryGlobal");
$app->get('/c/{parentCategoryUrl}/{categoryUrl}/', function (Request $request, Response $response, $args) {

    $path = $request->getUri()->getPath();
    $path = substr($path, 0, -1);
    return $this['response']
        ->withStatus(301)
        ->withHeader('Location', $path);

})->setName("categorySlash");
$app->get('/c/{parentCategoryUrl}/{categoryUrl}[/{page:[0-9]+}]', function (Request $request, Response $response, $args) {
    $productShower = new ProductShower($this->db);
    $headerCategories = new HeaderCategories($this->db);
    $import = filter_var($request->getAttribute('import', 'import'), FILTER_SANITIZE_STRING);
    $parentCategoryUrl = filter_var($args['parentCategoryUrl'], FILTER_SANITIZE_STRING);
    $categoryUrl = filter_var($args['categoryUrl'], FILTER_SANITIZE_STRING);
    $page = filter_var($request->getAttribute('page', 1), FILTER_SANITIZE_NUMBER_INT);
   // var_dump("category");
    $products = $productShower->getProductArray($import, $parentCategoryUrl, $categoryUrl, $page);
    $parentCategory = $productShower->getCategoryByUrlForRender($parentCategoryUrl);
    $currentCategory = $productShower->getCategoryByUrlForRender($categoryUrl);
    $categories = $productShower->getCategoryArray($import, $parentCategoryUrl, $categoryUrl);
    $pagination = $productShower->pagination($import, $parentCategoryUrl, $categoryUrl, $page);




    if($currentCategory['name'] == NULL){

        $redirector = new Redirector();
        $path = $request->getUri()->getPath();
        return $redirector->redirect($path,$this);
        //$response = $this->view->render($response, "notfound.phtml", ["router" => $this->router, "headerCategories" => $headerCategories->getGlobalCategories(),"externalConfig" => getExternalConfig()]);

        //return $response->withStatus(404);
    }
    else{
        if($page > 1 && count($products) == 0)
        {
            $path = "/c/".$parentCategoryUrl."/".$categoryUrl;
            return $this['response']
                ->withStatus(301)
                ->withHeader('Location', $path);
        }
        else{
            $whatPage = $import == 'import' ? "products-page.phtml" : "products-page-warehouse.phtml";
            $response = $this->view->render($response, $whatPage, [
                "router" => $this->router,
                "products" => $products,
                "pagination" => $pagination,
                "categories" => $categories,
                "category" => $currentCategory,
                "parentCategory" => $parentCategory,
                "headerCategories" => $headerCategories->getGlobalCategories(),
                "import" => $import,
                "currenturl" => getUrl($request),
                "externalConfig" => getExternalConfig()
            ]);
            return $response;
        }

    }


})->setName("category");
$app->get('/search', function (Request $request, Response $response, $args) {
    $productShower = new ProductShower($this->db);
    $headerCategories = new HeaderCategories($this->db);
    $allGetVars = $request->getQueryParams();
    $text = filter_var($allGetVars['name'], FILTER_SANITIZE_STRING);

    $products = $productShower->getSearchProductArray($text);
    //var_dump("search");
    $response = $this->view->render($response, "products-page-search.phtml", [
        "router" => $this->router,
        "products" => $products,
        "headerCategories" => $headerCategories->getGlobalCategories(),
        "currenturl" => getUrl($request),
        "import" => "import",
        "text" => $text,
        "externalConfig" => getExternalConfig()
    ]);
    return $response;
})->setName("search");



$app->get('/newsletter/{id}', function ($request, $response, $args) {
    $headerCategories = new HeaderCategories($this->db);
    $ticket_id = (int)$args['id'];
    $newsletterText = '';
    switch($ticket_id)
    {
        case 1 :
            $newsletterText = "Dziękujemy za zapis na listę";
            break;
        case 2 :
            $newsletterText = "Dziękujemy za zapis na listę. Prosimy o sprawdzenie skrzynki email. Prosimy o kliknięcie w link aktywacyjny.";
            break;
        case 3 :
            $newsletterText = "Dziękujemy za aktywację";
            break;
        case 5 :
            $newsletterText = "Odnotowaliśmy rezygnację";
            break;
        case 102 :
            $newsletterText = "Brak pola email w formularzu zapisu";
            break;
        case 103 :
            $newsletterText = "Brak wartości o polu dodatkowym";
            break;
        case 201 :
            $newsletterText = "Podany adres już jest na liście";
            break;
        case 202 :
            $newsletterText = "Błędny adres email";
            break;
        case 206 :
            $newsletterText = "Błąd przy aktywacji adresu email";
            break;
    }
    $response = $this->view->render($response, "newsletter-page.phtml", ["router" => $this->router,
        "newsletterText" => $newsletterText, "headerCategories" => $headerCategories->getGlobalCategories(),"breadcrumbtitle" => "Newsletter","externalConfig" => getExternalConfig()]);

    return $response;
});



//{ brand:"memorabbit","meta_description_pl": /.*DS\-0001.*/i, $or: [ { "name": /.*DS\-0001.*/i }, { $or: [ { "description_pl": /.*DS\-0001.*/i } ] } ] }

$app->run();


