module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        criticalcss: {
            custom: {
                options: {
                    url: "https://memorabbit.pl/c/pendrive-reklamowe/pendrive-o-indywidualnym-wygladzie",
                    width: 1920,
                    height: 1400,
                    outputfile: "src/public/critical.css",
                    filename: "src/public/maincss.css", // Using path.resolve( path.join( ... ) ) is a good idea here
                    buffer: 800*1024,
                    ignoreConsole: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-criticalcss');

    // Default task(s).
    grunt.registerTask('default', ['grunt-criticalcss']);

};